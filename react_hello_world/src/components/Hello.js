import React from 'react';

export default class Hello extends React.Component {
    constructor(props){
        super(props)
        this.state={
            users: ['Shu']
        }
    }

    render(){
        const { users } = this.state
        return(
            <ul>
                {users.map((user,index) => {
                    return (
                        <li key={index}>
                            Hello {user}
                        </li>
                    )
                })}
            </ul>
        )
    }
}